const webpack = require('webpack')

module.exports = {
  devtool: 'inline-source-map',
  // devtool: 'cheap-module-source-map',

  plugins: [
    new webpack.HotModuleReplacementPlugin(),

    new webpack.NamedModulesPlugin()
  ]
}
