const proxy = require('./proxy')
const webpackConfig = require('./webpack.common.conf')
// const historyApiFallback = require('./historyfallback')
const DEFAULT_PORT = parseInt(process.env.PORT, 10) || 3000
const HOST = process.env.HOST || '0.0.0.0'

module.exports = {
  port: DEFAULT_PORT,

  disableHostCheck: true,
  compress: true,
  clientLogLevel: 'none',
  hot: true,
  // 启用 quiet 后，除了初始启动信息之外的任何内容都不会被打印到控制台
  quiet: true,
  headers: {
    'access-control-allow-origin': '*'
  },
  publicPath: webpackConfig.output.publicPath,
  watchOptions: {
    ignored: /node_modules/
  },
  // historyApiFallback,
  overlay: false,
  host: HOST,
  proxy,
  https: !!process.env.HTTPS,
  contentBase: process.env.CONTENT_BASE
}
