var webpack = require('webpack')
var PurifyWebpack = require('purifycss-webpack')
var HtmlInlinkChunkPlugin = require('html-webpack-inline-chunk-plugin')
// var CleanWebpackPlugin = require('clean-webpack-plugin')

var path = require('path')
var glob = require('glob-all')

module.exports = {
  plugins: [
    new PurifyWebpack({
      paths: glob.sync([
        './client/*.html',
        './client/*.js'
      ])
    }),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest'
    }),

    new HtmlInlinkChunkPlugin({
      inlineChunks: ['manifest']
    }),

    new webpack.optimize.UglifyJsPlugin(),
  ]
}
