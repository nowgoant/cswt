import { JsonController, Get, Put, Post, Delete, Param, BodyParam, Body, Req, Res, NotFoundError } from 'routing-controllers';
import { Request, Response } from "express";
import User from '../models/User'
import { ConstraintMetadata } from 'class-validator/metadata/ConstraintMetadata';

@JsonController('/api/users')
export class ApiUserController {
  @Get("/sync")
  async sync(@Req() req: Request, @Res() res: Response) {
    await User.sync();
    res.sendStatus(200);
  }

  @Get('/create')
  async getUsersAction() {
    const user = await User.create({ name: 'majun', age: Math.floor(Math.random() * 100) });
    return user.dataValues;
  }

  @Get("/findAll")
  async findAll(@Req() req: Request, @Res() res: Response): Promise<any[]> {
    const user = await User.findAll();
    if (!user)
      return [];

    res.json(user);
  }

  @Get("/findById/:id")
  async getOne(@Param("id") id: number) {
    const user = await User.findById(id || '');
    if (!user)
      throw new NotFoundError(`User was not found.`);
    // console.log('user.dataValues', );
    return user.dataValues;
  }

  @Get("/update/:id")
  async update(@Req() req: Request, @Res() res: Response) {
    const updates = await User.update<User>({ name: 'majun1' }, { where: { id: req.params['id'] } });
    // return user.dataValues;
    res.send(`更新了${String(updates[0])}行`);
  }
}
