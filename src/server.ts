import * as express from "express";
import * as http from "http";
import * as bodyParser from "body-parser";
import * as compression from "compression";
import * as webpack from 'webpack';
import * as WebpackDevServer from 'webpack-dev-server';
import * as  ora from 'ora';
import chalk from 'chalk';

import { join } from "path";
import { useExpressServer, useContainer as rtUsec } from "routing-controllers";

import './sequelize';

const webapckConfig = require('./../build/webpack.common.conf.js');
const webapckDevServerConfig = require('./../build/webpack.dev.server.conf.js');
const HOST = process.env.HOST || '0.0.0.0';

export class Server {
  private app: any;
  private devServer: any;
  private port: any = 3000;
  private spinner: any;

  constructor() {
    let compiler: any;

    this.port = process.env.PORT || this.port;

    for (const key in webapckConfig.entry) {
      if (webapckConfig.entry.hasOwnProperty(key)) {
        const value = webapckConfig.entry[key];
        webapckConfig.entry[key] = [
          `webpack-dev-server/client?${process.env.HTTPS ? 'https' : 'http'}://${HOST}:${this.port}/`,
          'webpack/hot/dev-server',
          value
        ];
      }
    }

    const spinner = ora('start for server...')
    spinner.start()

    compiler = webpack(webapckConfig, (err, stats) => {
      spinner.stop();
      if (err) throw err
      process.stdout.write(stats.toString({
        colors: true,
        modules: false,
        children: false,// If you are using ts-loader, setting this to true will make TypeScript errors show up during build.
        chunks: false,
        chunkModules: false
      }) + '\n\n');

      this.onServerListen();

      if (stats.hasErrors()) {
        console.log(chalk.red('  Server failed with errors.\n'))
        process.exit(1)
      }

      console.log(chalk.cyan('Server complete.\n'))
    });

    const devServer = new WebpackDevServer(compiler, webapckDevServerConfig);

    this.devServer = devServer;
    this.app = devServer.app;
    this.app.use(compression());
    this.app.use(bodyParser.json()); // support json encoded bodies
    this.app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
  }

  public setRoutes = () => {
    useExpressServer(this.app, {
      /**
       * We can add options about how routing-controllers should configure itself.
       * Here we specify what controllers should be registered in our express server.
       */
      controllers: [join(__dirname, '/controllers/**/*')]
    });
  }

  public startServer = () => {
    // var httpServer = http.createServer(this.app);
    // httpServer.listen(this.port);
    // httpServer.on('error', this.onError);
    // httpServer.on('listening', this.onServerListen);
    this.devServer.listen(this.port, HOST, (err: any) => {
      if (err) {
        console.log(err);
        return;
      }

    });
  }

  //When hosting a client app such as angular - map the path to the client dist folder
  public setStaticFolders = () => {
    // var path = require('path');
    // let clientPath = path.join(__dirname, '../<client folder>/dist');
    //console.log(`adding static folder: ${clientPath}`)
    // this.app.use(express.static(clientPath));
  }

  private onServerListen = () => {
    console.log(chalk.cyan('App listening on port ' + this.port));
    if (process.env.NODE_ENV) {
      console.log(chalk.cyan("you are running in " + process.env.NODE_ENV + " mode."));
    }
  }

  onError = (err: any) => {
    switch (err.code) {
      case 'EACCES':
        console.error('port requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.error('port is already in use');
        process.exit(1);
        break;
      default:
        throw err;
    }
  }

  public setErrorHandlers = () => {
    // this.app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
    //   res.status((<any>err).status || 500);
    //   res.send({
    //     message: err.message,
    //     error: err
    //   });
    // });
  }
}
